/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.panneerat.finalproject;

import java.io.Serializable;

/**
 *
 * @author hp
 */
public class Dream implements Serializable{
    private String name;
    private String email;
    private String HotSCr;
    private String HotSBo;
    private String HotSCh;
    private String HelloFH;
    private String HelloFF;
    private String GlitchMSc;
    private String GlitchMGl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHotSCr() {
        return HotSCr;
    }

    public void setHotSCr(String HotSCr) {
        this.HotSCr = HotSCr;
    }

    public String getHotSBo() {
        return HotSBo;
    }

    public void setHotSBo(String HotSBo) {
        this.HotSBo = HotSBo;
    }

    public String getHotSCh() {
        return HotSCh;
    }

    public void setHotSCh(String HotSCh) {
        this.HotSCh = HotSCh;
    }

    public String getHelloFH() {
        return HelloFH;
    }

    public void setHelloFH(String HelloFH) {
        this.HelloFH = HelloFH;
    }

    public String getHelloFF() {
        return HelloFF;
    }

    public void setHelloFF(String HelloFF) {
        this.HelloFF = HelloFF;
    }

    public String getGlitchMSc() {
        return GlitchMSc;
    }

    public void setGlitchMSc(String GlitchMSc) {
        this.GlitchMSc = GlitchMSc;
    }

    public String getGlitchMGl() {
        return GlitchMGl;
    }

    public void setGlitchMGl(String GlitchMGl) {
        this.GlitchMGl = GlitchMGl;
    }

    public Dream(String name, String email, String HotSCr, String HotSBo, String HotSCh, String HelloFH, String HelloFF, String GlitchMSc, String GlitchMGl) {
        this.name = name;
        this.email = email;
        this.HotSCr = HotSCr;
        this.HotSBo = HotSBo;
        this.HotSCh = HotSCh;
        this.HelloFH = HelloFH;
        this.HelloFF = HelloFF;
        this.GlitchMSc = GlitchMSc;
        this.GlitchMGl = GlitchMGl;
    }

    @Override
    public String toString() {
        return "name: " + name + ", email: " + email + " | HotSause Crazy: " + HotSCr + ",  HotSause Boring: " + HotSBo + ",  HotSause Chilling: " + HotSCh + ",  HelloFuture Hello: " + HelloFH + ",  HelloFuture Future: " + HelloFF + ",  GlitchMode Scratch: "+ GlitchMSc + ",  GlitchMode Glitch: "+ GlitchMGl;
    }
}
