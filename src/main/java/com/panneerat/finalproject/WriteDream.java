/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.panneerat.finalproject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hp
 */
public class WriteDream {
    public static void main(String[] args) {
        ArrayList<Dream>dreams = new ArrayList();
        dreams.add(new Dream("kaew","panneerat.kaew@gmail.com","1","1","1","0","0","0","0"));
        dreams.add(new Dream("mark","marklee.02@sm.com","0","0","0","0","0","1","1"));
        dreams.add(new Dream("doyoung","dozazayoung.dy@sm.com","1","1","1","1","1","1","1"));
        
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("dream.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(dreams);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteDream.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteDream.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}
